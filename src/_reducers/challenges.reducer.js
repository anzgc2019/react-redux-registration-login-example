import { challengeConstants } from '../_constants';

const initialState = {
    all: {
        loading: false,
        error: false,
        data: []
    },
    detail: {
        loading: false,
        error: false,
        data: {
            id: 0,
            title: 'none selected'
        }
    }
};

export function challenges(state = initialState, action) {
    switch (action.type) {
      case challengeConstants.GETALL_REQUEST:
          return {
              ...state,
              all: { ...state.all, loading: true }
          };
      case challengeConstants.GETALL_SUCCESS:
          return {
              ...state,
              all: { ...state.all, loading: false, data: action.challenges }
          };
      case challengeConstants.GETALL_FAILURE:
          return {
              ...state,
              all: { ...state.all, loading: false, error: action.error }
          };
      case challengeConstants.GETINIT_REQUEST:
          return {
              ...state,
              detail: { ...state.detail, loading: true }
          };
      case challengeConstants.GETINIT_SUCCESS:
          return {
              ...state,
              detail: { ...state.detail, loading: false, data: action.challenge }
          };
      case challengeConstants.GETINIT_FAILURE:
          return {
              ...state,
              detail: { ...state.detail, loading: false, error: action.error }
          };
    case challengeConstants.DELETE_REQUEST:
      // add 'deleting:true' property to challenge being deleted
      return {
          ...state,
          all: {
              ...state.all,
                data: state.all.data.map(challenge =>
                    challenge.id === action.id
                        ? { ...challenge, deleting: true }
                        : challenge
                )
          }
      };
    case challengeConstants.DELETE_SUCCESS:
      // remove deleted challenge from state
          return {
              ...state,
              all: {
                  ...state.all,
                  data: state.data.filter(challenge => challenge.id !== action.id)
              }
      };
    case challengeConstants.DELETE_FAILURE:
      // remove 'deleting:true' property and add 'deleteError:[error]' property to challenge 
      return {
          ...state,
          all: {
              ...state.all,
              data: state.data.map(challenge => {
                  if (challenge.id === action.id) {
                      // make copy of challenge without 'deleting:true' property
                      const { deleting, ...challengeCopy } = challenge;
                      // return copy of challenge with 'deleteError:[error]' property
                      return { ...challengeCopy, deleteError: action.error };
                  }
                  else
                    return challenge;
              })
          }
      };
      default:
          return state;
  }
}