import { teamConstants } from '../_constants';

const initialState = {
    all: {
        loading: false,
        error: false,
        items: []
    },
    detail: {
        loading: false,
        error: false,
        data: {
            id: 0,
            title: 'none selected'
        }
    }
};

export function teams(state = initialState, action) {
    const copyOfAll = state.all;
    const copyOfDetail = state.detail;
  switch (action.type) {
      case teamConstants.GETALL_REQUEST:
          return {
              ...state,
              all: { ...copyOfAll, loading: true }
          };
      case teamConstants.GETALL_SUCCESS:
          return {
              ...state,
              all: { ...copyOfAll, loading: false, data: action.teams }
          };
      case teamConstants.GETALL_FAILURE:
          return {
              ...state,
              all: { ...copyOfAll, loading: false, error: action.error }
          };
      case teamConstants.GET_REQUEST:
          return {
              ...state,
              detail: { ...copyOfDetail, loading: true }
          };
      case teamConstants.GET_SUCCESS:
          return {
              ...state,
              detail: { ...copyOfDetail, loading: false, data: action.team }
          };
      case teamConstants.GET_FAILURE:
          return {
              ...state,
              detail: { ...copyOfDetail, loading: false, error: action.error }
          };
    case teamConstants.DELETE_REQUEST:
      // add 'deleting:true' property to team being deleted
      return {
        ...state,
        all: state.all.data.map(team =>
            team.id === action.id
                ? { ...team, deleting: true }
                : team
        )
      };
    case teamConstants.DELETE_SUCCESS:
      // remove deleted challenge from state
      return {
          ...state,
          all: state.all.data.filter(team => team.id !== action.id)
      };
    case teamConstants.DELETE_FAILURE:
      // remove 'deleting:true' property and add 'deleteError:[error]' property to team 
      return {
        ...state,
          all: state.all.data.map(team => {
              if (team.id === action.id) {
            // make copy of team without 'deleting:true' property
                  const { deleting, ...teamCopy } = team;
            // return copy of team with 'deleteError:[error]' property
                  return { ...teamCopy, deleteError: action.error };
          }

          return team;
        })
      };
      default:
          return state;
  }
}