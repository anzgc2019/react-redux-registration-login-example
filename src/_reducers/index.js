import { combineReducers } from 'redux';

import { authentication } from './authentication.reducer';
import { registration } from './registration.reducer';
import { alert } from './alert.reducer';
import { challenges } from './challenges.reducer';
import { quest } from './quest.reducer';
import { teams } from './teams.reducer';
import { users } from './users.reducer';

const rootReducer = combineReducers({
  authentication,
  registration,
  alert,
  challenges,
  quest,
  teams,
  users
});

export default rootReducer;