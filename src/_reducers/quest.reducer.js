import { questConstants } from '../_constants';

const initialState = {
    challenges: {
        loading: false,
        error: false,
        data: []
    },
    team: {
        loading: false,
        error: false,
        data: {
            id: 0,
            title: 'none selected'
        }
    }
};

export function quest(state = initialState, action) {
    switch (action.type) {
        case questConstants.INIT_TEAM_DETAILS:
            return {
                ...state,
                challenges: { ...state.challenges, loading: true }
            };
        case questConstants.GETINIT_REQUEST:
            return {
                ...state,
                team: { ...state.team, loading: true }
            };
        case questConstants.GETINIT_SUCCESS:
            return {
                ...state,
                team: { ...state.team, loading: false, data: action.team }
            };
        case questConstants.GETINIT_FAILURE:
            return {
                ...state,
                team: { ...state.team, loading: false, error: action.error }
            };
        case questConstants.GETALL_CHALLENGES_REQUEST:
            return {
                ...state,
                challenges: { ...state.challenges, loading: true }
            };
      case questConstants.GETALL_CHALLENGES_SUCCESS:
          return {
              ...state,
              challenges: { ...state.challenges, loading: false, data: action.challenges }
          };
      case questConstants.GETALL_CHALLENGES_FAILURE:
          return {
              ...state,
              challenges: { ...state.challenges, loading: false, error: action.error }
          };
        case questConstants.SET_ACTIVE_ASSIGNED_CHALLENGE_REQUEST:
            return {
                ...state,
                challenges: {
                    ...state.challenges,
                    data: state.challenges.data.map(challenge => 
                        challenge.assignedChallengeId === action.id
                            ? { ...challenge, active: true }
                            : { ...challenge, active: false }
                    ),
                    activeid: action.id
                }
            };
        case questConstants.SET_FORM_IN_ERROR:
            return {
                ...state,
                submission: { formError: true }
            };
        case questConstants.CLEAR_FORM_IN_ERROR:
            return {
                ...state,
                submission: { formError: false }
            };
        case questConstants.SUBMIT_CHALLENGE_ANSWER:
            return {
                ...state,
                submission: { loading: true }
            };
        case questConstants.SUBMIT_CHALLENGE_ANSWER_SUCCESS:
            var updatedChallengeData = state.challenges.data.map(challenge =>
                challenge.assignedChallengeId === action.data.result.assignedChallengeId
                    ? action.data.result
                    : challenge
            );
            if(action.data.nextChallenge)
                updatedChallengeData = [...updatedChallengeData, action.data.nextChallenge]
            return {
                ...state,
                submission: { loading: false, response: action.data },
                challenges: {
                    ...state.challenges,
                    data: updatedChallengeData,
                    activeid: action.data.result.score > 1 ? 0 : action.data.result.assignedChallengeId
                }
            };
        case questConstants.SUBMIT_CHALLENGE_ANSWER_FAILURE:
            return {
                ...state,
                submission: { loading: false, error: action.error }
            };
        case questConstants.UPDATE_STATS:
            // perform calculations here
            var stats = action.data.stats;
            var points = stats.basicPoints + stats.bonusPoints + stats.collaborationPoints - stats.penaltyPoints;
            return {
                ...state,
                stats: {
                    ...action.data.stats, overallPoints: points
                }
            };
            
      default:
          return state;
  }
}