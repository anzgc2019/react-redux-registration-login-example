import config from 'config';
import { authHeader } from '../_helpers';
import { userService } from '../_services';

export const teamService = {
    getAll,
    getById,
    getQuestByTeamId,
    submitAnswer,
    update,
    delete: _delete
};

function getAll() {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(`${config.apiUrl}/teams`, requestOptions).then(handleResponse);
}

function getById(id) {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(`${config.apiUrl}/teams/getbyid/${id}`, requestOptions).then(handleResponse);
}

function getQuestByTeamId(id) {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(`${config.apiUrl}/teams/getquest/${id}`, requestOptions).then(handleResponse);
}

function update(team) {
    const requestOptions = {
        method: 'PUT',
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: JSON.stringify(team)
    };

    return fetch(`${config.apiUrl}/teams/${team.id}`, requestOptions).then(handleResponse);
}

function submitAnswer(id, data) {
    const requestOptions = {
        method: 'POST',
        //headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: data
    };
    return fetch(`${config.apiUrl}/challenges/submitanswer/${id}`, requestOptions).then(handleResponse);

}

// prefixed function name with underscore because delete is a reserved word in javascript
function _delete(id) {
    const requestOptions = {
        method: 'DELETE',
        headers: authHeader()
    };

    return fetch(`${config.apiUrl}/teams/${id}`, requestOptions).then(handleResponse);
}

function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
                userService.logout();
                location.reload(true);
            }

            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}