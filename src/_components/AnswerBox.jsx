import React from 'react';
import { connect } from 'react-redux';
import { questActions } from '../_actions';

class AnswerBox extends React.Component {
    componentDidMount() {
        this.props.dispatch(questActions.setFormError(false));
    }
    handleSubmit(event) {
        const { quest } = this.props;
        event.preventDefault();
        if (!event.target.checkValidity()) {
            questActions.setFormError(true);
            return;
        }
        let data = new FormData(event.target);
        this.props.dispatch(questActions.setFormError(false));
        this.props.dispatch(questActions.submitAnswer(quest.challenges.activeid, data));
    }
    render() {
        const { quest } = this.props;
        return (
            <form noValidate aria-live="aggressive" onSubmit={this.handleSubmit.bind(this)} className={quest.submission && quest.submission.inError ? 'displayErrors' : ''}>
                <input id="form_challenge_submission" type="text" name="submission" required pattern="\w+" />
                <label htmlFor="form_challenge_submission"></label>
                <button>Submit</button>
            </form>
        );
    }
}

function mapStateToProps(state) {
    return {
        quest: state.quest
    };
}

const connectedAnswerBox = connect(mapStateToProps)(AnswerBox);
export { connectedAnswerBox as AnswerBox };