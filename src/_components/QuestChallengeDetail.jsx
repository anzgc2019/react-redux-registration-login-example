import React from 'react';
import { connect } from 'react-redux';
import { AnswerBox } from './AnswerBox';

class QuestChallengeDetail extends React.Component {
    componentDidMount() {
    }

    render() {
        const { challenge } = this.props;
        return (
            <div>
                <h3>Active Challenge:</h3>
                {challenge && <div>
                    {challenge.loading && <em>Loading challenge details...</em>}
                    {challenge.error && <span className="text-danger">ERROR: {challenge.error}</span>}
                    {
                        <div>
                            <h4>
                                {challenge.title}
                            </h4>
                            <ul>
                                <li>
                                    challengeId: {challenge.assignedChallengeId}
                                </li>
                                <li>
                                    {challenge.detail}
                                </li>
                                <li>
                                    basic points on offer: {challenge.offeredPoints}
                                </li>
                                <li>
                                    location: {challenge.mapReference}
                                </li>
                            </ul>
                        </div>
                    }
                    <AnswerBox/>
                </div>}
            </div>
        );
    }

}

function mapStateToProps(state) {
    if (state && state.quest && state.quest.challenges && state.quest.challenges.activeid) {
        return {
            challenge: state.quest.challenges.data.filter(challenge => challenge.assignedChallengeId === state.quest.challenges.activeid)[0],
            //challenge: state.quest.challenges.data.filter(challenge => challenge.active === true)[0],
            activeid: state.quest.challenges.activeid
        };
    } else {
        return {
            notfound: true
        };
    }
}

const connectedQuestChallengeDetail = connect(mapStateToProps)(QuestChallengeDetail);
export { connectedQuestChallengeDetail as QuestChallengeDetail };