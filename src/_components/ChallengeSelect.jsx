import React from 'react';
import { connect } from 'react-redux';
import { challengeActions } from '../_actions';

class ChallengeSelect extends React.Component {
    componentDidMount() {
        this.props.dispatch(challengeActions.getAll());
    }

    render() {
        const { challenges } = this.props;
        if (challenges) {
            return (
                <div>
                    <h3>All registered challenges:</h3>
                    {challenges.loading && <em>Loading challenges...</em>}
                    {challenges.error && <span className="text-danger">ERROR: {challenges.error}</span>}
                    {
                        challenges.data &&
                        <ul>
                            {challenges.data.map((challenge, index) =>
                                <li key={challenge.id} onClick={() => { this.props.dispatch(challengeActions.getById(challenge.id)) }}>
                                    {challenge.title}
                                </li>
                            )}
                        </ul>
                    }
                </div>
            );
        }
        return (
            <div>
                <h3>Hmmm....</h3>
            </div>
        );
    }

}

function mapStateToProps(state) {
    return {
        challenges: state.challenges.all
    };
}

const connectedChallengeSelect = connect(mapStateToProps)(ChallengeSelect);
export { connectedChallengeSelect as ChallengeSelect };