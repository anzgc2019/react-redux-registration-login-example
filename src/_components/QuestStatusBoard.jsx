import React from 'react';
import { connect } from 'react-redux';
import { AnswerBox } from './AnswerBox';

class QuestStatusBoard extends React.Component {
    componentDidMount() {
    }

    render() {
        const { stats } = this.props;
        return (
            <div>
                <h3>Status:</h3>
                {stats && <div>
                    {stats.updating && <em>Updating status...</em>}
                    {stats.error && <span className="text-danger">ERROR: {stats.error}</span>}
                    {
                        <div>
                            <h4>
                                Current Score: {stats.overallPoints}
                            </h4>
                            <ul>
                                <li>
                                    Basic: {stats.basicPoints}
                                </li>
                                <li>
                                    Bonus: {stats.bonusPoints}
                                </li>
                                <li>
                                    Collaboration: {stats.collaborationPoints}
                                </li>
                                <li>
                                    Penalty: {stats.penaltyPoints}
                                </li>
                            </ul>
                        </div>
                    }
                </div>}
            </div>
        );
    }

}

function mapStateToProps(state) {
    if (state && state.quest && state.quest.stats) {
        return {
            stats: state.quest.stats
        };
    } else {
        return {
            notfound: true
        };
    }
}

const connectedQuestStatusBoard = connect(mapStateToProps)(QuestStatusBoard);
export { connectedQuestStatusBoard as QuestStatusBoard };