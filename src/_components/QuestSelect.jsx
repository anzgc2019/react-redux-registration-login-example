import React from 'react';
import { connect } from 'react-redux';
import { questActions } from '../_actions';
import { teamActions } from '../_actions';

class QuestSelect extends React.Component {
    componentDidMount() {
        //this.props.dispatch(teamActions.getAll());
    }

    render() {
        const { quest } = this.props;
        if (quest) {
            return (
                <div>
                    <h3>Challenges:</h3>
                    {quest.loading && <em>Loading quest...</em>}
                    {quest.error && <span className="text-danger">ERROR: {quest.error}</span>}
                    {
                        quest.challenges && quest.challenges.data &&
                        <ul>
                            {quest.challenges.data.map((challenge, index) =>
                                <li key={challenge.assignedChallengeId} className={challenge.completed ? "completed" : "open"}
                                    onClick={() => { this.props.dispatch(questActions.setActiveAssignedChallenge(challenge.assignedChallengeId)) }}>
                                    {challenge.title} [{challenge.score}]
                                </li>
                            )}
                        </ul>
                    }
                </div>
            );
        }
        return (
            <div>
                <h3>Hmmm....</h3>
            </div>
        );
    }

}

function mapStateToProps(state) {
    return {
        quest: state.quest
    };
}

const connectedQuest = connect(mapStateToProps)(QuestSelect);
export { connectedQuest as QuestSelect };