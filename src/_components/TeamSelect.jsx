import React from 'react';
import { connect } from 'react-redux';
import { teamActions } from '../_actions';

class TeamSelect extends React.Component {
    componentDidMount() {
        this.props.dispatch(teamActions.getAll());
    }

    render() {
        const { teams } = this.props;
        if (teams) {
            return (
                <div>
                    <h3>All registered teams:</h3>
                    {teams.loading && <em>Loading teams...</em>}
                    {teams.error && <span className="text-danger">ERROR: {teams.error}</span>}
                    {
                        teams.data &&
                        <ul>
                            {teams.data.map((team, index) =>
                                <li key={team.id} onClick={() => { this.props.dispatch(teamActions.getById(team.id)) }}>
                                    {team.title}
                                </li>
                            )}
                        </ul>
                    }
                </div>
            );
        }
        return (
            <div>
                <h3>Hmmm....</h3>
            </div>
        );
    }

}

function mapStateToProps(state) {
    return {
        teams: state.teams.all
    };
}

const connectedTeamSelect = connect(mapStateToProps)(TeamSelect);
export { connectedTeamSelect as TeamSelect };