import React from 'react';
import { connect } from 'react-redux';

class ChallengeDetail extends React.Component {
    componentDidMount() {
    }

    render() {
        const { challenge } = this.props;
        return (
            <div>
                <h3>Challenge Details:</h3>
                {challenge && <div>
                    {challenge.loading && <em>Loading challenge details...</em>}
                    {challenge.error && <span className="text-danger">ERROR: {challenge.error}</span>}
                    {
                        challenge && challenge.data &&
                        <ul>
                            <li key={challenge.data.id}>
                                {challenge.data.title}
                                {challenge.data.title}
                                {challenge.data.title}
                            </li>
                        </ul>
                    }
                </div>}
            </div>
        );
    }

}

function mapStateToProps(state) {
    const { challenges } = state;
    return {
        challenge: challenges.detail
    };
}

const connectedChallengeDetail = connect(mapStateToProps)(ChallengeDetail);
export { connectedChallengeDetail as ChallengeDetail };