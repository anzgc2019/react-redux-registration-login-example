import React from 'react';
import { connect } from 'react-redux';
import { teamActions } from '../_actions';
import { questActions } from '../_actions';

class TeamDetail extends React.Component {
    componentDidMount() {
        this.props.dispatch(teamActions.getById(1));
    }

    render() {
        const { team } = this.props;
        return (
            <div>
                <h3>Team Details:</h3>
                {team && <div>
                    {team.loading && <em>Loading team details...</em>}
                    {team.error && <span className="text-danger">ERROR: {team.error}</span>}
                    {
                        team && !team.loading && !team.error &&
                        <ul>
                            <li key={team.data.id}>
                                {team.data.title} <span onClick={() => { this.props.dispatch(questActions.Initialise(team.data.id)) }}>[Start Quest]</span>
                            </li>
                        </ul>
                    }
                </div>}
            </div>
        );
    }

}

function mapStateToProps(state) {
    const { teams } = state;
    return {
        team: teams.detail
    };
}

const connectedTeamDetail = connect(mapStateToProps)(TeamDetail);
export { connectedTeamDetail as TeamDetail };