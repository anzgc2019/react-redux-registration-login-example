import React from 'react';
import { connect } from 'react-redux';
import { teamActions } from '../_actions';
import { QuestSelect } from './QuestSelect';
import { QuestStatusBoard } from './QuestStatusBoard';
import { QuestChallengeDetail } from './QuestChallengeDetail';

class QuestPanel extends React.Component {
    componentDidMount() {
        //this.props.dispatch(teamActions.getAll());
    }

    render() {
        const { quest } = this.props;
        if (quest) {
            return (
                <div>
                    <h3>Quest of {quest.team.data.title}:</h3>
                    <QuestStatusBoard />
                    <QuestSelect />
                    <QuestChallengeDetail />
                </div>
            );
        }
        return (
            <div>
                <h3>Hmmm....</h3>
            </div>
        );
    }

}

function mapStateToProps(state) {
    return {
        quest: state.quest
    };
}

const connectedQuest = connect(mapStateToProps)(QuestPanel);
export { connectedQuest as QuestPanel };