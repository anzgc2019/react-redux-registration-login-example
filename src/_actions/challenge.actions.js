import { challengeConstants } from '../_constants';
import { challengeService } from '../_services';
import { alertActions } from './';
import { history } from '../_helpers';

export const challengeActions = {
    getAll,
    getById,
    getInitial,
    delete: _delete
};

function getAll() {
    return dispatch => {
        dispatch(request());

        challengeService.getAll()
            .then(
                challenges => dispatch(success(challenges)),
                error => dispatch(failure(error.toString()))
            );
    };

    function request() { return { type: challengeConstants.GETALL_REQUEST } }
    function success(challenges) { return { type: challengeConstants.GETALL_SUCCESS, challenges } }
    function failure(error) { return { type: challengeConstants.GETALL_FAILURE, error } }
}

function getById(id) {
    return dispatch => {
        dispatch(request(id));

        challengeService.getById(id)
            .then(
                challenge => dispatch(success(challenge)),
                error => dispatch(failure(error.toString()))
            );
    };

    function request(id) { return { type: challengeConstants.GETBYID_REQUEST, id } }
    function success(challenge) { return { type: challengeConstants.GETBYID_SUCCESS, challenge } }
    function failure(error) { return { type: challengeConstants.GETBYID_FAILURE, error } }
}

function getInitial(id) {
    return dispatch => {
        dispatch(request(id));

        challengeService.getByTeamId(id)
            .then(
                challenges => dispatch(success(challenges)),
                error => dispatch(failure(error.toString()))
            );
    };

    function request(id) { return { type: challengeConstants.GETINIT_REQUEST, id } }
    function success(challenges) { return { type: challengeConstants.GETINIT_SUCCESS, challenges } }
    function failure(error) { return { type: challengeConstants.GETINIT_FAILURE, error } }
}

// prefixed function name with underscore because delete is a reserved word in javascript
function _delete(id) {
    return dispatch => {
        dispatch(request(id));

        challengeService.delete(id)
            .then(
                challenge => dispatch(success(id)),
                error => dispatch(failure(id, error.toString()))
            );
    };

    function request(id) { return { type: challengeConstants.DELETE_REQUEST, id } }
    function success(id) { return { type: challengeConstants.DELETE_SUCCESS, id } }
    function failure(id, error) { return { type: challengeConstants.DELETE_FAILURE, id, error } }
}