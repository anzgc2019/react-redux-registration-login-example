import { teamConstants } from '../_constants';
import { teamService } from '../_services';
import { alertActions } from './';
import { history } from '../_helpers';

export const teamActions = {
    getAll,
    getById,
    delete: _delete
};

function getAll() {
    return dispatch => {
        dispatch(request());

        teamService.getAll()
            .then(
                teams => {
                    dispatch(success(teams));
                    dispatch(alertActions.success("Please select your team."));
                    },
                error => dispatch(failure(error.toString()))
            );
    };

    function request() { return { type: teamConstants.GETALL_REQUEST } }
    function success(teams) { return { type: teamConstants.GETALL_SUCCESS, teams } }
    function failure(error) { return { type: teamConstants.GETALL_FAILURE, error } }
}

function getById(id) {
    return dispatch => {
        dispatch(request(id));

        teamService.getById(id)
            .then(
            team => {
                dispatch(success(team));
                dispatch(alertActions.success("Hit [Start Quest] when ready."));
            },
                error => dispatch(failure(error.toString()))
            );
    };

    function request(id) { return { type: teamConstants.GET_REQUEST, id } }
    function success(team) { return { type: teamConstants.GET_SUCCESS, team } }
    function failure(error) { return { type: teamConstants.GET_FAILURE, error } }
}

// prefixed function name with underscore because delete is a reserved word in javascript
function _delete(id) {
    return dispatch => {
        dispatch(request(id));

        teamService.delete(id)
            .then(
                team => dispatch(success(id)),
                error => dispatch(failure(id, error.toString()))
            );
    };

    function request(id) { return { type: teamConstants.DELETE_REQUEST, id } }
    function success(id) { return { type: teamConstants.DELETE_SUCCESS, id } }
    function failure(id, error) { return { type: teamConstants.DELETE_FAILURE, id, error } }
}