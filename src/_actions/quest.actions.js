import { questConstants } from '../_constants';
import { challengeService, teamService, questService } from '../_services';
import { alertActions } from './';
import { history } from '../_helpers';

export const questActions = {
    Initialise,
    setActiveAssignedChallenge,
    setFormError,
    submitAnswer
};

function Initialise(id) {
    return dispatch => {
        dispatch(request_team(id));
        teamService.getById(id)
            .then(
            team => {
                dispatch(success_team(team));
                dispatch(alertActions.success("Select your first challenge. Good luck!"));
            },
            error => {
                dispatch(failure_team(error.toString()));
                dispatch(alertActions.error(error.toString()));
            }
        );
        
        dispatch(request_challenges(id));
        challengeService.getByTeam(id)
            .then(
            challenges => dispatch(success_challenges(challenges)),
            error => {
                dispatch(failure_challenges(error.toString()));
                dispatch(alertActions.error(error.toString()));
            }
            );
            
    };

    function request_team(id) { return { type: questConstants.GETINIT_REQUEST, id } }
    function success_team(team) { return { type: questConstants.GETINIT_SUCCESS, team } }
    function failure_team(error) { return { type: questConstants.GETINIT_FAILURE, error } }

    function request_challenges(id) { return { type: questConstants.GETALL_CHALLENGES_REQUEST, id } }
    function success_challenges(challenges) { return { type: questConstants.GETALL_CHALLENGES_SUCCESS, challenges } }
    function failure_challenges(error) { return { type: questConstants.GETALL_CHALLENGES_FAILURE, error } }
}

function setActiveAssignedChallenge(id) {
    return dispatch => {
        dispatch(request(id));
        dispatch(alertActions.success("New challenge selected! Tick Tock"));
    };

    function request(id) { return { type: questConstants.SET_ACTIVE_ASSIGNED_CHALLENGE_REQUEST, id } }
}

function setFormError(inError) {
    return dispatch => {
        if (inError)
            return dispatch({ type: questConstants.SET_FORM_IN_ERROR, inError: true });
        else
            return dispatch({ type: questConstants.CLEAR_FORM_IN_ERROR, inError: false });
    };
}


function submitAnswer(id, formdata) {
    return dispatch => {
        dispatch(request(id));
        
        questService.submitAnswer(id, formdata)
            .then(
            submission_result => {
                dispatch(success(submission_result));
                dispatch(updateStats(submission_result));
            },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
            
    };

    function updateStats(data) { return { type: questConstants.UPDATE_STATS, data } }

    function request(id) { return { type: questConstants.SUBMIT_CHALLENGE_ANSWER, id } }
    function success(data) { return { type: questConstants.SUBMIT_CHALLENGE_ANSWER_SUCCESS, data } }
    function failure(error) { return { type: questConstants.SUBMIT_CHALLENGE_ANSWER_FAILURE, error } }
}


