import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { challengeActions } from '../_actions';

class ChallengePage extends React.Component {
    componentDidMount() {
        this.props.dispatch(challengeActions.getAll());
    }

    handleDeleteChallenge(id) {
        return (e) => this.props.dispatch(challengeActions.delete(id));
    }

    render() {
        const { user, challenges } = this.props;
        return (
            <div className="col-md-6 col-md-offset-3">
                {challenges.loading && <em>Loading challenges...</em>}
                {challenges.error && <span className="text-danger">ERROR: {challenges.error}</span>}
                {challenges.items &&
                   <ul>
                        {challenges.items.map((challenge, index) =>
                        <li key={challenge.id}> { index + ': ' + challenge.title + ' ' + challenge.basicPoints } </li>
                        )}
                    </ul>
                }
                <p>
                    <Link to="/login">Logout</Link>
                </p>
                <p>
                    <span><a onClick={() => { this.props.dispatch(challengeActions.getInitial(1)) }}>Load Initial Challenges</a></span>
                </p>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { challenges, authentication } = state;
    const { user } = authentication;
    return {
        user,
        challenges
    };
}

const connectedChallengePage = connect(mapStateToProps)(ChallengePage);
export { connectedChallengePage as ChallengePage };